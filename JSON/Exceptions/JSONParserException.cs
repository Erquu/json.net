﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JSONdotNET
{
    public class JSONParserException : Exception
    {
        public JSONParserException() : base() { }
        public JSONParserException(string message) : base(message) { }
    }
}
