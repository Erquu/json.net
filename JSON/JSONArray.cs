﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JSONdotNET
{
    /// <summary>
    /// Class representing a json array
    /// </summary>
    public class JSONArray : JSONObject, IEnumerator<JSONObject>
    {
        /// <summary>A list of JSON Objects</summary>
        private List<JSONObject> objects = new List<JSONObject>();
        /// <summary>The index for the enumerator implementation</summary>
        private int index = -1;

        /// <summary>Gets the number of objects in this array</summary>
        public int Length { get { return objects.Count; } }

        /// <summary>
        /// Returns the object at the given index
        /// </summary>
        /// <param name="index">The zero based index</param>
        /// <returns>The value at the given index</returns>
        public JSONObject this[int index] { get { return objects[index]; } }

        /// <inheritdoc />
        public override JSONObjectType Type { get { return JSONObjectType.ARRAY; } }

        public JSONObject Current
        {
            get
            {
                try
                {
                    return objects[index];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        object System.Collections.IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        /// <summary>Adds a JSONObject to the array</summary>
        /// <param name="obj">The object to be added</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(JSONObject obj)
        {
            objects.Add(obj);
            return this;
        }
        /// <summary>Adds a string to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(string value)
        {
            objects.Add(new JSONTextNode(value));
            return this;
        }
        /// <summary>Adds a boolean to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(bool value)
        {
            objects.Add(new JSONLiteralNode(value.ToString().ToLower()));
            return this;
        }
        /// <summary>Adds a 8 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(byte value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a 16 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(short value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds an unsigned 16 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(ushort value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a 32 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(int value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds an unsigned 32 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(uint value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a 64 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(long value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds an unsigned 64 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(ulong value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a single precision binary floating point number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(float value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a double precision binary floating point number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(double value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }

        /// <inheritdoc />
        internal override string Prettify(int depth, JSONPrettifierSettings settings)
        {
            string depthString = GetDepthString(depth, settings);
            StringBuilder builder = new StringBuilder();

            bool minified = settings.Minified;

            builder.Append("[");
            if (!minified)
            {
                builder.Append(settings.LineFeed);
                builder.Append(depthString);
            }
            int i = 0;
            foreach (JSONObject obj in objects)
            {
                builder.Append(obj.Prettify(depth + 1, settings));
                if (++i < objects.Count)
                    builder.Append(",");
                if (!minified)
                {
                    builder.Append(settings.LineFeed);
                    if (i < objects.Count)
                    {
                        builder.Append(depthString);
                    }
                }
            }
            if (!minified)
                builder.Append(GetDepthString(depth - 1, settings));
            builder.Append("]");
            return builder.ToString();
        }

        /// <summary>Removes all elements from the List</summary>
        public void Clear()
        {
            objects.Clear();
        }

        public IEnumerator<JSONObject> GetEnumerator()
        {
            return objects.GetEnumerator();
        }

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            return (++index) < Length;
        }

        public void Reset()
        {
            index = -1;
        }
    }
}