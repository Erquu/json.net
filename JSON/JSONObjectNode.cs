﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace JSONdotNET
{
    public class JSONObjectNode : JSONObject
    {
        /// <summary>The key-value pairs/content of the object</summary>
        private Dictionary<string, JSONObject> values = new Dictionary<string, JSONObject>();

        /// <summary>Retreives the value for a given key from this object</summary>
        /// <param name="key">The key for the object</param>
        /// <returns>The value for the given key</returns>
        public virtual JSONObject this[string key] { get { return values[key]; } }

        /// <summary>
        /// Checks if the objects contains a pair with the given key
        /// </summary>
        /// <param name="key">The key to check</param>
        /// <returns>True if containing key</returns>
        public virtual bool ContainsKey(string key)
        {
            return values.ContainsKey(key);
        }

        /// <summary>
        /// Removes a pair by the given key
        /// </summary>
        /// <param name="key">The key for the entry to remove</param>
        public virtual void Remove(string key)
        {
            values.Remove(key);
        }

        /// <summary>Retreives the value for a given key from this object</summary>
        /// <param name="key">The key for the object</param>
        /// <returns>The value for the given key</returns>
        public JSONObject Get(string key)
        {
            return values[key];
        }
        /// <summary>
        /// <para>Retreives the value for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public JSONObject Get(params object[] keys)
        {
            JSONObject returnValue = this;
            for (int i = 0; i < keys.Length; i++)
            {
                if (keys[i] is string && returnValue is JSONObjectNode)
                    returnValue = ((JSONObjectNode)returnValue).Get(keys[i].ToString());
                else if (keys[i] is Int32 && returnValue is JSONArrayNode)
                    returnValue = ((JSONArrayNode)returnValue)[(int)keys[i]];

                if (returnValue == null)
                {
                    break;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// <para>Retreives the value as a string for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public string GetString(string key)
        {
            return Get(key).ToString();
        }
        /// <summary>
        /// <para>Retreives the value as a string for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public string GetString(params string[] key)
        {
            JSONObject obj = Get(key);
            if (obj != null)
                return obj.ToString();
            return null;
        }

        /// <summary>
        /// <para>Retreives the value as a 16 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public short GetShort(string key)
        {
            return Int16.Parse(GetString(key));
        }
        /// <summary>
        /// <para>Retreives the value as a 16 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public short GetShort(params string[] key)
        {
            return Int16.Parse(GetString(key));
        }

        /// <summary>
        /// <para>Retreives the value as a 32 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public int GetInt(string key)
        {
            return Int32.Parse(GetString(key));
        }
        /// <summary>
        /// <para>Retreives the value as a 32 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public int GetInt(params string[] key)
        {
            return Int32.Parse(GetString(key));
        }

        /// <summary>
        /// <para>Retreives the value as a 64 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public long GetLong(string key)
        {
            return Int64.Parse(GetString(key));
        }
        /// <summary>
        /// <para>Retreives the value as a 64 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public long GetLong(params string[] key)
        {
            return Int64.Parse(GetString(key));
        }

        /// <summary>
        /// <para>Retreives the value as a single precision binary floating point number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public float GetFloat(string key)
        {
            return float.Parse(GetString(key), CultureInfo.InvariantCulture);
        }
        /// <summary>
        /// <para>Retreives the value as a single precision binary floating point number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public float GetFloat(params string[] key)
        {
            return GetFloat(GetString(key));
        }

        /// <summary>
        /// <para>Retreives the value as a double precision binary floating point number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public double GetDouble(string key)
        {
            return Double.Parse(GetString(key), CultureInfo.InvariantCulture);
        }
        /// <summary>
        /// <para>Retreives the value as a double precision binary floating point number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public double GetDouble(params string[] key)
        {
            return GetDouble(GetString(key));
        }

        /// <summary>
        /// <para>Retreives the value as a boolean for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public bool GetBool(string key)
        {
            return Boolean.Parse(GetString(key));
        }
        /// <summary>
        /// <para>Retreives the value as a boolean for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public bool GetBool(params string[] key)
        {
            return Boolean.Parse(GetString(key));
        }

        /// <summary>Adds a JSONObject with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The instance of the JSONObject</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, JSONObject value)
        {
            values.Add(key, value);
            return this;
        }
        /// <summary>
        /// <para>Adds a string with the given key to the object/node</para>
        /// Can be null
        /// </summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, string value)
        {
            if (value == null)
                return Add(key, new JSONLiteralNode("null"));
            return Add(key, new JSONTextNode(value));
        }
        /// <summary>Adds a boolean with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, bool value)
        {
            return Add(key, new JSONLiteralNode(value.ToString()));
        }
        /// <summary>Adds a 16 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, short value)
        {
            return Add(key, new JSONLiteralNode(value.ToString()));
        }
        /// <summary>Adds a 32 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, int value)
        {
            return Add(key, new JSONLiteralNode(value.ToString()));
        }
        /// <summary>Adds a 64 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, long value)
        {
            return Add(key, new JSONLiteralNode(value.ToString()));
        }
        /// <summary>Adds a double precision binary floating point number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, double value)
        {
            return Add(key, new JSONLiteralNode(value.ToString(CultureInfo.InvariantCulture)));
        }
        /// <summary>Adds a single precision binary floating point number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, float value)
        {
            return Add(key, new JSONLiteralNode(value.ToString(CultureInfo.InvariantCulture)));
        }

        /// <summary>Creates a well formed or minified JSON string</summary>
        /// <param name="depth">The base indent depth</param>
        /// <param name="minified">Indicates if the document should be minified</param>
        /// <returns>A valid json document</returns>
        public override string ToJSONString(int depth, bool minified)
        {
            string depthString = GetDepthString(depth);
            StringBuilder builder = new StringBuilder();

            builder.Append("{");
            if (!minified)
            {
                builder.Append("\n");
                builder.Append(depthString);
            }
            IEnumerator<string> keys = values.Keys.GetEnumerator();
            int i = 0;
            while (keys.MoveNext())
            {
                string key = keys.Current;
                builder.Append(String.Format("\"{0}\": ", ToValidJSONString(key)));
                builder.Append(values[key].ToJSONString(depth + 1, minified));
                if (++i < values.Count)
                    builder.Append(",");
                if (!minified)
                {
                    builder.Append("\n");
                    if (i < values.Count)
                    {
                        builder.Append(depthString);
                    }
                }
            }
            if (!minified)
                builder.Append(GetDepthString(depth - 1));
            builder.Append("}");
            return builder.ToString();
        }
    }
}
