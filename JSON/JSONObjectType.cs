﻿
namespace JSONdotNET
{
    public enum JSONObjectType
    {
        OBJECT,
        ARRAY,
        LITERAL,
        TEXT
    }
}
