﻿using System.IO;
using System.Text;

namespace JSONdotNET
{
    /// <summary>
    /// To become at least a bit as comfortable as the javascript JSON prototype, this class will allow static calls on JSONObject.Parse :)
    /// </summary>
    public class JSON
    {
        private static JSONParser PARSER { get { return new JSONParser(); } }

        /// <summary>Parses a json document</summary>
        /// <param name="file">The file to parse</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(FileInfo file)
        {
            return PARSER.Parse(file);
        }
        /// <summary>Parses a json document</summary>
        /// <param name="file">The file to parse</param>
        /// <param name="encoding">The streams encoding</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(FileInfo file, Encoding encoding)
        {
            return PARSER.Parse(file, encoding);
        }
        /// <summary>Parses a json document</summary>
        /// <param name="stream">Document input stream to read from</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(Stream stream)
        {
            return PARSER.Parse(stream);
        }
        /// <summary>Parses a json document</summary>
        /// <param name="stream">Document input stream to read from</param>
        /// <param name="encoding">The streams encoding</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(Stream stream, Encoding encoding)
        {
            return PARSER.Parse(stream, encoding);
        }
        /// <summary>Parses a json document</summary>
        /// <param name="doc">The document to be parsed</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(string doc)
        {
            return PARSER.Parse(doc);
        }
    }

    /// <summary>
    /// The JSONParser is a simple and very fast parser for well formed json documents
    /// If the document is not valid the result may be different than expected without any exception thrown
    /// </summary>
    public class JSONParser
    {
        private JSONTokenizer tokenizer = new JSONTokenizer();

        public JSONObject Parse(string json)
        {
            this.tokenizer.Tokenize(json);

            return this.Parse();
        }
        public JSONObject Parse(FileInfo file)
        {
            this.tokenizer.Tokenize(file);

            return this.Parse();
        }
        public JSONObject Parse(FileInfo file, Encoding encoding)
        {
            this.tokenizer.Tokenize(file, encoding);

            return this.Parse();
        }
        public JSONObject Parse(Stream stream)
        {
            this.tokenizer.Tokenize(stream);

            return this.Parse();
        }
        public JSONObject Parse(Stream stream, Encoding encoding)
        {
            this.tokenizer.Tokenize(stream, encoding);

            return this.Parse();
        }

        private JSONObject Parse()
        {
            JSONObject root = null;

            while (this.tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                if (this.tokenizer.Token == JSONToken.START_OBJECT)
                {
                    root = this.ParseObject();
                }
                else if (this.tokenizer.Token == JSONToken.START_ARRAY)
                {
                    root = this.ParseArray();
                }
            }

            return root;
        }

        private JSONObject ParseObject()
        {
            JSONObject current = new JSONObject();
            string currentKey = null;

            while (this.tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                switch (this.tokenizer.Token)
                {
                    case JSONToken.FIELD_NAME:
                        currentKey = this.tokenizer.TokenValue;
                        break;
                    case JSONToken.FIELD_VALUE_STRING:
                        current.Add(currentKey, new JSONTextNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.FIELD_VALUE_NULL:
                        current.Add(currentKey, new JSONLiteralNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.FIELD_VALUE_TRUE:
                        current.Add(currentKey, new JSONLiteralNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.FIELD_VALUE_FALSE:
                        current.Add(currentKey, new JSONLiteralNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.FIELD_VALUE_INTEGER:
                    case JSONToken.FIELD_VALUE_DOUBLE:
                        current.Add(currentKey, new JSONLiteralNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.START_ARRAY:
                        current.Add(currentKey, this.ParseArray());
                        break;
                    case JSONToken.START_OBJECT:
                        current.Add(currentKey, this.ParseObject());
                        break;
                    case JSONToken.END_OBJECT:
                        return current;
                }
            }

            return current;
        }

        private JSONObject ParseArray()
        {
            JSONArray current = new JSONArray();

            while (this.tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                switch (this.tokenizer.Token)
                {
                    case JSONToken.FIELD_VALUE_STRING:
                        current.Add(new JSONTextNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.FIELD_VALUE_NULL:
                        current.Add(new JSONLiteralNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.FIELD_VALUE_TRUE:
                        current.Add(new JSONLiteralNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.FIELD_VALUE_FALSE:
                        current.Add(new JSONLiteralNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.FIELD_VALUE_INTEGER:
                    case JSONToken.FIELD_VALUE_DOUBLE:
                        current.Add(new JSONLiteralNode(this.tokenizer.TokenValue));
                        break;
                    case JSONToken.START_ARRAY:
                        current.Add(this.ParseArray());
                        break;
                    case JSONToken.START_OBJECT:
                        current.Add(this.ParseObject());
                        break;
                    case JSONToken.END_ARRAY:
                        return current;
                }
            }
            return current;
        }
    }
}
