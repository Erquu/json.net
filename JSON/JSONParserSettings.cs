﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JSONdotNET
{
    /**
     * 1 = 1
     * 2 = 2
     * 4 = 4
     * 8 = 8
     * 16 = 10
     * 32 = 20
     * 64 = 40
     * 128 = 80
     * 256 = 100
     * 512 = 200
     * 1024 = 400
     * 2048 = 800
     * 4096 = 1000
     * 8192 = 2000
     */

    [Flags]
    public enum JSONParserSettings
    {
        /// <summary>
        /// 
        /// </summary>
        NONE = 0x01,

        /// <summary>
        /// When set, the <paramref name="JSONTokenizer"/> will automatically close the given stream
        /// </summary>
        AUTO_CLOSE_SOURCE = 0x02,

        /// <summary>
        /// When set, the <paramref name="JSONTokenizer"/> will push tokens via the <see cref="JSONTokenizer#TokenFound"/> event
        /// </summary>
        TOKEN_PUSH = 0x04,

        /// <summary>
        /// When set, <see cref="JSONParserSettings.TOKEN_PUSH"/> will be automatically set
        /// </summary>
        ASYNC = 0x08,

        /// <summary>
        /// When set, JSONTokenizer will handle whitespace tokens instead of ignoring them
        /// </summary>
        HANDLE_WHITESPACE = 0x10,

        /// <summary>
        /// When set, JSONTokenizer will handle single line comments as a valid token
        /// <example>
        /// //foo bar
        /// will result in the token
        /// Token: COMMENT
        /// Value: "foo bar"
        /// </example>
        /// </summary>
        ALLOW_C_LIKE_LINE_COMMENTS = 0x20,

        /// <summary>
        /// When set, JSONTokenizer will handle multiline comments as a valid token
        /// <example>
        /// /*foo bar*/
        /// will result in the token
        /// Token: COMMENT
        /// Value: "foo bar"
        /// </example>
        /// </summary>
        ALLOW_C_LIKE_MULTILINE_COMMENTS = 0x40,

        /// <summary>
        /// When set, JSONTokenizer won't throw an error when string contains a line break (\n)
        /// </summary>
        ALLOW_MULTILINE_STRINGS = 0x80
    }
}
