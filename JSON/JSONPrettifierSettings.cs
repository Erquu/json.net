﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JSONdotNET
{
    public class JSONPrettifierSettings
    {
        public static JSONPrettifierSettings DEFAULT = new JSONPrettifierSettings("  ", Environment.NewLine, false, true);

        /// <summary>Gets or sets the option if non ASCII characters should be escaped (\u...)</summary>
        public bool ConvertNonASCII { get; set; }
        /// <summary>Gets or sets the sequence used to indent when stringifying json objects</summary>
        public string IndentSequence { get; set; }
        /// <summary>Gets or sets the sequence used to end lines</summary>
        public string LineFeed { get; set; }
        /// <summary>Gets or sets the option to minify the json output, removes unnescessary whitespaces and linebreaks</summary>
        public bool Minified { get; set; }

        /// <summary>
        /// Creates a new instance by inheriting options from another JSONPrettifierSettings instance
        /// </summary>
        /// <param name="inherit">The JSONPrettifierSettings instance to inherit from</param>
        public JSONPrettifierSettings(JSONPrettifierSettings inherit) : this(
            inherit.IndentSequence,
            inherit.LineFeed,
            inherit.Minified,
            inherit.ConvertNonASCII)
        { }
        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="indentSequence">The sequence used to indent when stringifying json objects</param>
        /// <param name="lineFeed">The sequence used to end lines</param>
        /// <param name="minified">Boolean indicating if the stringify output should be minified</param>
        public JSONPrettifierSettings(string indentSequence, string lineFeed, bool minified) : this(
            indentSequence,
            lineFeed,
            minified,
            JSONPrettifierSettings.DEFAULT.ConvertNonASCII)
        { }
        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="indentSequence">The sequence used to indent when stringifying json objects</param>
        /// <param name="lineFeed">The sequence used to end lines</param>
        /// <param name="minified">Boolean indicating if the stringify output should be minified</param>
        /// <param name="convertNonASCII">Boolean indicating if non ascii charactes should be escaped (\u...)</param>
        public JSONPrettifierSettings(string indentSequence, string lineFeed, bool minified, bool convertNonASCII)
        {
            this.IndentSequence = indentSequence;
            this.LineFeed = lineFeed;
            this.Minified = minified;
            this.ConvertNonASCII = convertNonASCII;
        }
    }
}
