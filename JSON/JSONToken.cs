﻿namespace JSONdotNET
{
    public enum JSONToken
    {
        /// <summary>Default token</summary>
        UNKNOWN,

        /// <summary>Token for the start of an object</summary>
        START_OBJECT,

        /// <summary>Token for the end of an object</summary>
        END_OBJECT,

        /// <summary>Token for the start of an array</summary>
        START_ARRAY,

        /// <summary>Token for the end of an array</summary>
        END_ARRAY,

        /// <summary>Token for JSON keys</summary>
        FIELD_NAME,

        /// <summary>Generic token for other values</summary>
        FIELD_VALUE,

        /// <summary>Token for a string value</summary>
        FIELD_VALUE_STRING,

        /// <summary>Token for an integer</summary>
        FIELD_VALUE_INTEGER,

        /// <summary>Token for a floating point number</summary>
        FIELD_VALUE_DOUBLE,

        /// <summary>Token for "true" literal value</summary>
        FIELD_VALUE_TRUE,

        /// <summary>Token for "false" literal value</summary>
        FIELD_VALUE_FALSE,

        /// <summary>Token for "null" literal value</summary>
        FIELD_VALUE_NULL,

        /// <summary>
        /// Token for single or multiline comment if
        /// JSONParserSettings.ALLOW_C_LIKE_LINE_COMMENTS orJSONParserSettings.ALLOW_C_LIKE_MULTILINE_COMMENTS is set</summary>
        COMMENT,

        /// <summary>Token for end of file/stream</summary>
        END_OF_FILE,

        /// <summary>Token for whitespace characters</summary>
        WHITESPACE,

        /// <summary>Alias for all FIELD_VALUE_* fields</summary>
        VALUES = FIELD_VALUE | FIELD_VALUE_STRING | FIELD_VALUE_INTEGER | FIELD_VALUE_DOUBLE | FIELD_VALUE_TRUE | FIELD_VALUE_FALSE | FIELD_VALUE_NULL
    }
}
