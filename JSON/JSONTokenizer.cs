﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace JSONdotNET
{
    public delegate void TokenFoundEventHandler(object sender, JSONToken token);
    
    public class JSONTokenizer
    {
        /// <summary>Enum to identify the type of the structure we're in</summary>
        private enum MODE
        {
            /// <summary>
            /// Array type
            /// <example>
            /// [ "value" ]
            /// </example>
            /// </summary>
            ARRAY,

            /// <summary>
            /// Object type
            /// <example>
            /// { "key": "value" }
            /// </example>
            /// </summary>
            OBJECT
        }

        /// <summary>
        /// Event for found tokens
        /// Will be fired only if <see cref="JSONParserSettings.TOKEN_PUSH"/> or
        /// <see cref="JSONParserSettings.ASYNC"/> is set
        /// </summary>
        public event TokenFoundEventHandler TokenFound;

        /// <summary>Settings flag(s) for the current tokenizer</summary>
        private JSONParserSettings settings;
        /// <summary>Stream representation of the JSON document</summary>
        private Stream source;
        /// <summary>StreamReader of the source stream</summary>
        private StreamReader reader;

        /// <summary>
        /// When peeking the next character using <see cref="JSONTokenizer#PeekCharacter()"/>,
        /// the char will not be consumed (unless you peek again)
        /// </summary>
        private char? peek = null;

        /// <summary>The last token value found or null if empty or no token was found (yet)</summary>
        private string lastTokenValue = null;
        /// <summary>The last token found or the default JSONToken value if no token was found (yet)</summary>
        private JSONToken lastFoundToken = default(JSONToken);

        /// <summary>Stack of <see cref="MODE"/> to keep track of the json structure if it has child nodes</summary>
        private Stack<MODE> mode = new Stack<MODE>();

        /// <summary>Indicator the see if the following character sequence should be a key</summary>
        private bool isKey = false;
        /// <summary>Indicator the see if the last found character sequence was a value</summary>
        private bool lastWasValue = false;

        /// <summary>Contains the last read character in index 0 and the current character in index 1</summary>
        private char[] buffer;

        /// <summary>The current column of the char pointer (to provide more helpful error messages)</summary>
        private int pointerColumn;
        /// <summary>The current line of the char pointer (to provide more helpful error messages)</summary>
        private int pointerLine;

        /// <summary>
        /// Returns the last found token
        /// </summary>
        public JSONToken Token
        {
            get { return this.lastFoundToken; }
        }

        /// <summary>
        /// Returns the last found token value
        /// </summary>
        public string TokenValue
        {
            get { return this.lastTokenValue; }
        }

        public JSONTokenizer()
        { }

        /// <returns>Returns the next token</returns>
        /// <exception cref="InvalidOperationException">When setting <see cref="JSONParserSettings.TOKEN_PUSH"/> it is not allowed to pull tokens</exception>
        public JSONToken NextToken()
        {
            if (this.IsSet(JSONParserSettings.TOKEN_PUSH))
            {
                throw new InvalidOperationException("NextToken() is only allowed when JSONParserSettings.TOKEN_PUSH is not set");
            }

            //Console.WriteLine("Searching for next token in Line {0} Column {1}", this.pointerLine, this.pointerColumn);
            JSONToken token;
            do
            {
                token = this.GetNextToken();
            } while ((token == JSONToken.WHITESPACE && !this.IsSet(JSONParserSettings.HANDLE_WHITESPACE)));

            return token;
        }

        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="json">The json document</param>
        public void Tokenize(string json)
        {
            this.Tokenize(json, 0);
        }
        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="file">The FileInfo of the json document</param>
        public void Tokenize(FileInfo file)
        {
            this.Tokenize(file, 0);
        }
        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="source">The stream to read the json document from</param>
        public void Tokenize(Stream source)
        {
            this.Tokenize(source, Encoding.Default);
        }
        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="json">The json document</param>
        /// <param name="settings">The parser settings to use</param>
        public void Tokenize(string json, JSONParserSettings settings)
        {
            if (json == null)
            {
                throw new ArgumentNullException("json is null");
            }

            Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(json));

            if ((settings & JSONParserSettings.AUTO_CLOSE_SOURCE) != JSONParserSettings.AUTO_CLOSE_SOURCE)
            {
                settings |= JSONParserSettings.AUTO_CLOSE_SOURCE;
            }

            this.Tokenize(stream, settings);
        }
        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="file">The FileInfo of the json document</param>
        /// <param name="encoding">The character encoding to use</param>
        public void Tokenize(FileInfo file, Encoding encoding)
        {
            this.Tokenize(file, 0, encoding);
        }
        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="file">The FileInfo of the json document</param>
        /// <param name="settings">The parser settings to use</param>
        public void Tokenize(FileInfo file, JSONParserSettings settings)
        {
            this.Tokenize(file, settings, Encoding.Default);
        }
        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="file">The FileInfo of the json document</param>
        /// <param name="settings">The parser settings to use</param>
        /// <param name="encoding">The character encoding to use</param>
        public void Tokenize(FileInfo file, JSONParserSettings settings, Encoding encoding)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file is null");
            }

            if (!file.Exists)
            {
                throw new FileNotFoundException("file does not exist");
            }

            if ((settings & JSONParserSettings.AUTO_CLOSE_SOURCE) != JSONParserSettings.AUTO_CLOSE_SOURCE)
            {
                settings |= JSONParserSettings.AUTO_CLOSE_SOURCE;
            }

            Stream stream = file.OpenRead();

            this.Tokenize(stream, settings, encoding);
        }
        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="source">The source stream of the document</param>
        /// <param name="encoding">The character encoding to use</param>
        public void Tokenize(Stream source, Encoding encoding)
        {
            this.Tokenize(source, 0, encoding);
        }
        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="source">The source stream of the document</param>
        /// <param name="settings">The parser setings to use</param>
        public void Tokenize(Stream source, JSONParserSettings settings)
        {
            this.Tokenize(source, settings, Encoding.Default);
        }
        /// <summary>
        /// Tokenizes a JSON document
        /// </summary>
        /// <param name="source">The source stream of the document</param>
        /// <param name="settings">The parser settings to use</param>
        /// <param name="encoding">The character encoding to use</param>
        public void Tokenize(Stream source, JSONParserSettings settings, Encoding encoding)
        {
            this.Reset();

            if (source == null)
            {
                throw new ArgumentNullException("source is null");
            }

            this.settings = settings;
            this.source = source;
            this.reader = new StreamReader(this.source, encoding);

            if (this.IsSet(JSONParserSettings.ASYNC) && !this.IsSet(JSONParserSettings.TOKEN_PUSH))
            {
                this.settings |= JSONParserSettings.TOKEN_PUSH;
            }

            if (this.IsSet(JSONParserSettings.ASYNC))
            {
                Thread tokenizerThread = new Thread(new ThreadStart(ThreadRunner));
                tokenizerThread.Name = "TokenizerThread";
                tokenizerThread.IsBackground = true;
                tokenizerThread.Start();
            }
            else if (this.IsSet(JSONParserSettings.TOKEN_PUSH))
            {
                ThreadRunner();
            }
        }

        /// <summary>
        /// Resets all the nescessary fields for Tokenizing another document
        /// </summary>
        private void Reset()
        {
            this.mode.Clear();

            this.buffer = new char[2];
            this.pointerColumn = 0;
            this.pointerLine = 1;

            this.peek = null;
            this.isKey = false;
            this.lastWasValue = false;

            this.lastFoundToken = default(JSONToken);

            if (this.source != null && this.IsSet(JSONParserSettings.AUTO_CLOSE_SOURCE))
                this.source.Close();
            this.source = null;

            if (this.reader != null)
            {
                if (this.IsSet(JSONParserSettings.AUTO_CLOSE_SOURCE))
                {
                    this.reader.Close();
                }
                this.reader.Dispose();
            }
            this.reader = null;
        }

        /// <summary>
        /// Scans the stream for the next token
        /// </summary>
        /// <returns>The found token</returns>
        private JSONToken GetNextToken()
        {
            string tokenValue = null;
            JSONToken foundToken = JSONToken.UNKNOWN;

            if (this.NextCharacter() != ushort.MaxValue) // TODO: Is this the best way to identify EOF
            {
                char current = this.buffer[1];

                switch (current)
                {
                    case '{':
                        isKey = true;

                        this.mode.Push(MODE.OBJECT);
                        foundToken = JSONToken.START_OBJECT;
                        break;
                    case '}':
                        {
                            if (this.mode.Count > 0)
                            {
                                MODE cMode = this.mode.Pop();
                                if (cMode == MODE.OBJECT)
                                {
                                    foundToken = JSONToken.END_OBJECT;
                                }
                                else
                                {
                                    throw new JSONParserException(this.FormatExceptionText("Expected \"]\" but found \"}\""));
                                }
                            }
                            else
                            {
                                throw new JSONParserException(this.FormatExceptionText("Unexpected \"}\""));
                            }
                        }
                        break;
                    case '[':
                        isKey = false;

                        this.mode.Push(MODE.ARRAY);
                        foundToken = JSONToken.START_ARRAY;
                        break;
                    case ']':
                        {
                            if (this.mode.Count > 0)
                            {
                                MODE cMode = this.mode.Pop();
                                if (cMode == MODE.ARRAY)
                                {
                                    foundToken = JSONToken.END_ARRAY;
                                }
                                else
                                {
                                    throw new JSONParserException(this.FormatExceptionText("Expected \"}\" but found \"]\""));
                                }
                            }
                            else
                            {
                                throw new JSONParserException(this.FormatExceptionText("Unexpected \"]\""));
                            }
                        }
                        break;
                    case '"':
                        if (!this.lastWasValue)
                        {
                            tokenValue = this.ScanString();
                            foundToken = this.isKey ? JSONToken.FIELD_NAME : JSONToken.FIELD_VALUE_STRING;

                            this.lastWasValue = true;
                        }
                        else
                        {
                            throw new JSONParserException("Unexpected string sequence");
                        }
                        break;
                    case ':':
                        if (this.isKey)
                        {
                            this.lastWasValue = false;
                            this.isKey = false;
                            return this.GetNextToken();
                        }
                        else
                        {
                            throw new JSONParserException(this.FormatExceptionText("Unexpected \":\""));
                        }
//                        break; // break not needed here
                    case ',':
                        if (!this.isKey && this.lastWasValue)
                        {
                            this.lastWasValue = false;
                            this.isKey = (this.mode.Peek() == MODE.OBJECT);
                            return this.GetNextToken();
                        }
                        else
                        {
                            throw new JSONParserException(this.FormatExceptionText("Unexpected \",\""));
                        }
//                        break; // break not needed here
                    case '/':
                        bool singleLineCommentAllowed = this.IsSet(JSONParserSettings.ALLOW_C_LIKE_LINE_COMMENTS);
                        bool multiLineCommentAllowed = this.IsSet(JSONParserSettings.ALLOW_C_LIKE_MULTILINE_COMMENTS);

                        if (singleLineCommentAllowed || multiLineCommentAllowed)
                        {
                            char next = this.PeekCharacter();
                            if (singleLineCommentAllowed && next == '/')
                            {
                                foundToken = JSONToken.COMMENT;
                                tokenValue = this.ScanSingleLineComment();
                            }
                            else if (multiLineCommentAllowed && next == '*')
                            {
                                this.NextCharacter();
                                foundToken = JSONToken.COMMENT;
                                tokenValue = this.ScanMultilineComment();
                            }
                            else
                            {
                                // TODO: The error message is okay but could be better
                                throw new JSONParserException(this.FormatExceptionText("Expected Comment but found literal"));
                            }
                        }
                        else
                        {
                            throw new JSONParserException(this.FormatExceptionText("Unexpected \"/\""));
                        }
                        break;
                    default:
                        if (Char.IsWhiteSpace(current))
                        {
                            if (current == '\n')
                            {
                                this.pointerColumn = 0;
                                this.pointerLine++;
                            }

                            foundToken = JSONToken.WHITESPACE;
                        }
                        else
                        {
                            if (!this.isKey)
                            {
                                if (!this.lastWasValue)
                                {
                                    tokenValue = this.ScanLiteral(current, out foundToken);
                                    this.lastWasValue = true;
                                }
                                else
                                {
                                    throw new JSONParserException("Unexpected literal");
                                }
                            }
                            else 
                            {
                                throw new JSONParserException(this.FormatExceptionText("Expected string but found literal"));
                            }
                        }
                        break;
                }
            }
            else
            {
                if (this.IsSet(JSONParserSettings.AUTO_CLOSE_SOURCE))
                {
                    this.reader.DiscardBufferedData();
                    this.reader.Close();
                    this.reader.Dispose();
                    this.reader = null;

                    this.source.Close();
                    this.source.Dispose();
                    this.source = null;
                }

                foundToken = JSONToken.END_OF_FILE;
            }

            return this.HandleTokenFound(foundToken, tokenValue);
        }

        /// <summary>
        /// Reads a json literal value from the stream until it is finished
        /// </summary>
        /// <param name="current">The beginning char of the literal</param>
        /// <param name="type">The exact JSONToken.FIELD_VALUE_* type</param>
        /// <returns>The found literal as a string</returns>
        private string ScanLiteral(char current, out JSONToken type)
        {
            if (current == 't')
            {
                if (this.PeekCharacters(3) == "rue")
                {
                    this.NextCharacter();

                    type = JSONToken.FIELD_VALUE_TRUE;
                    return "true";
                }
                else
                {
                    throw new JSONParserException(this.FormatExceptionText("Unknown literal found"));
                }
            }
            else if (current == 'f')
            {
                if (this.PeekCharacters(4) == "alse")
                {
                    this.NextCharacter();

                    type = JSONToken.FIELD_VALUE_FALSE;
                    return "false";
                }
                else
                {
                    throw new JSONParserException(this.FormatExceptionText("Unknown literal found"));
                }
            }
            else if (current == 'n')
            {
                if (this.PeekCharacters(3) == "ull")
                {
                    this.NextCharacter();

                    type = JSONToken.FIELD_VALUE_NULL;
                    return "null";
                }
                else
                {
                    throw new JSONParserException(this.FormatExceptionText("Unknown literal found"));
                }
            }
            else
            {
                StringBuilder numberBuilder = new StringBuilder();

                type = JSONToken.FIELD_VALUE_INTEGER;

                if (current == '-')
                {
                    numberBuilder.Append(current);
                    current = this.PeekCharacter();
                }

                if (current == '0')
                {
                    numberBuilder.Append(current);
                    current = this.PeekCharacter();
                }
                else if (current >= '1' && current <= '9')
                {
                    do
                    {
                        numberBuilder.Append(current);
                    } while (Char.IsNumber((current = this.PeekCharacter())));
                }
                else
                {
                    throw new FormatException(this.FormatExceptionText("Expected number character but found {0}", current));
                }

                if (current == '.')
                {
                    type = JSONToken.FIELD_VALUE_DOUBLE;

                    do
                    {
                        numberBuilder.Append(current);
                    } while (Char.IsNumber((current = this.PeekCharacter())));
                }

                if (current == 'e' || current == 'E')
                {
                    numberBuilder.Append(current);
                    current = this.PeekCharacter();

                    if (current == '-' || current == '+')
                    {
                        numberBuilder.Append(current);
                        current = this.PeekCharacter();
                    }

                    do
                    {
                        numberBuilder.Append(current);
                    } while (Char.IsNumber((current = this.PeekCharacter())));
                }

                return numberBuilder.ToString();
            }
        }

        /// <returns>The content of a single line comment</returns>
        private string ScanSingleLineComment()
        {
            StringBuilder commentBuilder = new StringBuilder();

            char current;
            while ((current = this.PeekCharacter()) != '\n')
            {
                commentBuilder.Append(current);
            }
            return commentBuilder.ToString();
        }

        /// <returns>The content of a multiline comment</returns>
        private string ScanMultilineComment()
        {
            StringBuilder commentBuilder = new StringBuilder();
            char current;

            while (true)
            {
                current = this.NextCharacter();
                if (current == '*' && this.PeekCharacter() == '/')
                {
                    this.NextCharacter();
                    break;
                }
                else if (current == ushort.MaxValue)
                {
                    throw new JSONParserException(this.FormatExceptionText("Multiline comment was never closed"));
                }
                else
                {
                    commentBuilder.Append(current);
                }
            }

            return commentBuilder.ToString();
        }

        /// <summary>
        /// Reads a json string value from the stream until it is finished
        /// </summary>
        /// <returns>The found string value</returns>
        /// <exception cref="JSONParserException">A hex escape sequence was found but has an invalid char</exception>
        private string ScanString()
        {
            StringBuilder b = new StringBuilder();

            bool run = true;

            while (run && this.NextCharacter() != ushort.MaxValue) // \0 ?
            {
                char current = this.buffer[1];

                if (Char.IsLetterOrDigit(current))
                {
                    b.Append(current);
                }
                else
                {
                    if (current == '\\')
                    {
                        char next = this.NextCharacter();
                        switch (next)
                        {
                            case '"':
                            case '\\':
                            case '/':
                                 b.Append(next);
                                 break;
                            case 'b': b.Append('\b'); break;
                            case 'f': b.Append('\f'); break;
                            case 'n': b.Append('\n'); break;
                            case 'r': b.Append('\r'); break;
                            case 't': b.Append('\t'); break;
                            case 'u':
                                StringBuilder digits = new StringBuilder(4);
                                for (int i = 0; i < 4; i++)
                                {
                                    next = this.NextCharacter();
                                    if (this.IsHex(next))
                                    {
                                        digits.Append(next);
                                    }
                                    else
                                    {
                                        throw new JSONParserException(this.FormatExceptionText("Expected hex character but found {0}", next));
                                    }
                                }

                                int unicodeChar = Int32.Parse(digits.ToString(), System.Globalization.NumberStyles.HexNumber);
                                string c = Char.ConvertFromUtf32(unicodeChar);
                                b.Append(c);
                                break;
                            default:
                                throw new JSONParserException(this.FormatExceptionText("Unexpected escape character"));
                                //break;
                        }
                    }
                    else if (current == '"')
                    {
                        break;
                    }
                    else if (current == '\n' && !this.IsSet(JSONParserSettings.ALLOW_MULTILINE_STRINGS))
                    {
                        throw new JSONParserException(this.FormatExceptionText("Unexpected end of line"));
                    }
                    else
                    {
                        b.Append(current);
                    }
                }
            }

            return b.ToString();
        }

        /// <summary>Checks if a character is a valid hex char</summary>
        /// <param name="c">The character to check</param>
        /// <returns>true if the given char is a valid hex char</returns>
        private bool IsHex(char c)
        {
            return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
        }

        /// <summary>Loads the next char from the stream into the buffer</summary>
        /// <returns>The next character or ushort.MaxValue</returns>
        private char NextCharacter()
        {
            this.pointerColumn++;

            this.buffer[0] = this.buffer[1];
            if (this.peek != null)
            {
                char peeked = (char)this.peek;
                this.buffer[1] = peeked;
                this.peek = null;

                return peeked;
            }
            else
            {
                return this.buffer[1] = (char)this.reader.Read();
            }
        }

        // The function is a bit different from the "default" peek functionallity
        // known from e.g. InputStream but in this case most of the times a character is 
        // peeked and depending on the case can be consumed and the next character needs
        // to be peeked
        /// <summary>
        /// <para>
        /// Peeks the next character without consuming it 
        /// for the first time the function is called after calling NextCharacter()
        /// </para>
        /// <para>
        /// Multiple calls in a row will consume the previously peeked character
        /// </para>
        /// </summary>
        /// <returns>The next character in the input stream</returns>
        private char PeekCharacter()
        {
            if (this.peek != null)
            {
                this.pointerColumn++;
            }

            char next = (char)this.reader.Read();
            this.peek = next;

            return next;
        }

        /// <summary>
        /// Reads the 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private string PeekCharacters(int count)
        {
            StringBuilder sb = new StringBuilder(count);

            for (int i = 0; i < count; i++)
            {
                sb.Append(this.PeekCharacter());
            }

            return sb.ToString();
        }

        /// <summary>
        /// Checks if a given <see cref="JSONParserSettings"/> flag is set
        /// </summary>
        /// <param name="test">The <see cref="JSONParserSettings"/> flag to check</param>
        /// <returns>True if the option is set</returns>
        private bool IsSet(JSONParserSettings test)
        {
            return (this.settings & test) == test;
        }

        /// <summary>
        /// Function to set all required fields if a token was found
        /// If JSONParserSettings.TOKEN_PUSH is set the eventhandler will be invoked by this function
        /// </summary>
        /// <param name="token">The found token</param>
        /// <param name="value">The value of the found token, can be null</param>
        /// <returns>The token found</returns>
        private JSONToken HandleTokenFound(JSONToken token, string value)
        {
            this.lastFoundToken = token;
            this.lastTokenValue = value;

            //Console.WriteLine(this.lastTokenValue);
            
            if (this.IsSet(JSONParserSettings.TOKEN_PUSH) &&
                this.TokenFound != null)
            {
                if (!(token == JSONToken.WHITESPACE && !this.IsSet(JSONParserSettings.HANDLE_WHITESPACE)))
                    this.TokenFound.Invoke(this, token);
            }

            return token;
        }
        
        /// <summary>
        /// Adds the current line number and column to the given text
        /// </summary>
        /// <param name="format">A composite format string</param>
        /// <param name="args">A System.Object array containing zero or more objects to format</param>
        /// <returns>The formatted exception string</returns>
        private string FormatExceptionText(string format, params object[] args)
        {
            return this.FormatExceptionText(String.Format(format, args));
        }
        /// <summary>
        /// Adds the current line number and column to the given text
        /// </summary>
        /// <param name="text">The text to show</param>
        /// <returns>The formatted exception string</returns>
        private string FormatExceptionText(string text)
        {
            return String.Format("{0} in line {1} column {2}", text, this.pointerLine, this.pointerColumn);
        }

        /// <summary>
        /// Thread delegeate for when JSONParserSettings.ASYNC ist set
        /// </summary>
        private void ThreadRunner()
        {
            while (this.GetNextToken() != JSONToken.END_OF_FILE) ;
        }
    }
}
