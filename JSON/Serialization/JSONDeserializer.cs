﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace JSONdotNET.Serialization
{
    public class JSONDeserializer
    {
        public T Deserialize<T>(JSONObject o)
        {
            object instance;
            ConvertToType(o, typeof(T), out instance);

            return (T)instance;
        }

        private bool AssignToPropertyOrField(object value, object instance, string memberName)
        {
            Type instanceType = instance.GetType();

            PropertyInfo propInfo = instanceType.GetProperty(memberName, BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public);
            if (propInfo != null)
            {
                MethodInfo setter = propInfo.GetSetMethod();
                if (setter != null)
                {
                    //Type propType = propInfo.PropertyType;
                    //if (ConvertToType(value, propType, out value))
                    //{
                        try
                        {
                            setter.Invoke(instance, new object[] { value });
                            return true;
                        }
                        catch
                        {
                            // TODO: throw exception because value can not be set
                            return false;
                        }
                    //}
                }
            }

            FieldInfo fieldInfo = instanceType.GetField(memberName, BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public);
            if (fieldInfo != null)
            {
                //Type fieldType = fieldInfo.FieldType;
                //if (ConvertToType(null, fieldType, out value))
                //{
                    try
                    {
                        fieldInfo.SetValue(instance, value);
                        return true;
                    }
                    catch
                    {
                        // TODO: throw exception because value can not be set
                        return false;
                    }
                //}
            }

            return true;
        }

        /* The difference between ConvertJSONObject and ConvertToType is, that this on is only for real JSONObjects (Key,Value)
         * and not one of it's sub classes like JSONArray or JSONLiteralNode
         */
        private bool ConvertJSONObject(JSONObject o, Type t, out object converted)
        {
            Type targetType = null;

            if (o.ContainsKey("_type"))
            {
                string typeName = o.GetString("_type");
                targetType = ResolveType(typeName);
            }
            if (targetType == null)
            {
                targetType = t;
            }

            if (targetType != null)
            {
                if (targetType != null && IsInstantiatable(targetType))
                {
                    if (IsGenericDictionary(targetType))
                    {
                        Type keyType = targetType.GetGenericArguments()[0];
                        if (keyType != typeof(string) && keyType != typeof(object))
                        {
                            // TODO: throw exception because key can not be set
                            converted = null;
                            return false;
                        }

                        Type valueType = targetType.GetGenericArguments()[1];
                        IDictionary dict = null;
                        if (IsInstantiatable(targetType))
                        {
                            dict = (IDictionary)Activator.CreateInstance(targetType);
                        }
                        else
                        {
                            t = (typeof(Dictionary<,>)).MakeGenericType(keyType, valueType);
                            dict = (IDictionary)Activator.CreateInstance(t);
                        }

                        if (t != null)
                        {
                            foreach (string memberName in o.Keys)
                            {
                                object memberObject;
                                if (ConvertToType(o[memberName], valueType, out memberObject))
                                {
                                    dict[memberName] = memberObject;
                                }
                                else
                                {
                                    converted = null;
                                    return false;
                                }
                            }

                            converted = dict;
                            return true;
                        }
                    }

                    object instance = Activator.CreateInstance(targetType);

                    foreach (string memberName in o.Keys)
                    {
                        Type memberType = null;

                        PropertyInfo pi = targetType.GetProperty(memberName);
                        if (pi == null)
                        {
                            FieldInfo fi = targetType.GetField(memberName);
                            if (fi == null)
                            {
                                continue;
                            }
                            memberType = fi.FieldType;
                        }
                        else
                        {
                            memberType = pi.PropertyType;
                        }


                        object value;
                        if (ConvertToType(o[memberName], memberType, out value))
                        {
                            if (!AssignToPropertyOrField(value, instance, memberName))
                            {
                                converted = null;
                                return false;
                            }
                        }
                    }

                    converted = instance;
                    return true;
                }
            }

            converted = null;
            return false;
        }

        private bool ConvertJSONArray(JSONArray a, Type t, out object converted)
        {
            converted = null;
            return false;
        }

        private bool ConvertToType(JSONObject o, Type t, out object converted)
        {
            if (o == null)
            {
                if (!Nullable(t))
                {
                    // TODO: throw exception if requested type does not accept null values
                }

                converted = null;
                return true;
            }

            if (o is JSONLiteralNode)
            {
                TypeConverter tc = TypeDescriptor.GetConverter(t);
                if (tc.CanConvertFrom(typeof(string)))
                {
                    try
                    {
                        converted = tc.ConvertFrom(((JSONLiteralNode)o).GetString());
                        return true;
                    }
                    catch
                    {
                        converted = null;
                        return false;
                    }
                }
                else
                {
                    // TODO: throw exception, could not convert literal
                }
            }
            else if (o is JSONArray)
            {
                return ConvertJSONObject((JSONArray)o, t, out converted);
            }
            else
            {
                return ConvertJSONObject(o, t, out converted);
            }

            converted = null;
            return false;
        }

        private Type ResolveType(string typeName)
        {
            try
            {
                return Type.GetType(typeName);
            }
            catch
            {
                return null;
            }
        }

        private bool IsInstantiatable(Type t)
        {
            if (t == null || t.IsAbstract || t.IsInterface || t.IsArray)
            {
                return false;
            }

            if (t == typeof(object))
            {
                return false;
            }

            if (t.IsValueType)
            {
                return true;
            }

            ConstructorInfo ci = t.GetConstructor(BindingFlags.Public | BindingFlags.Instance, null, new Type[0], null);
            return (ci != null);
        }

        private bool IsGenericDictionary(Type type)
        {
            return type != null &&
                type.IsGenericType &&
                (typeof(IDictionary).IsAssignableFrom(type) || type.GetGenericTypeDefinition() == typeof(IDictionary<,>)) &&
                type.GetGenericArguments().Length == 2;
        }

        private bool Nullable(Type t)
        {
            return (t != null && t.IsValueType && t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));
        }
    }
}
