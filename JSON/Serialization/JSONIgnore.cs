﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JSONdotNET.Serialization
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple=false, Inherited=true)]
    public sealed class JSONIgnore : Attribute
    {
        public JSONIgnore()
        { }
    }
}
