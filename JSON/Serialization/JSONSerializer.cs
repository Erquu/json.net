﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace JSONdotNET.Serialization
{
    class ReferenceComparator : IEqualityComparer
    {
        public bool Equals(object x, object y)
        {
            return x == y;
        }

        public int GetHashCode(object obj)
        {
            return System.Runtime.CompilerServices.RuntimeHelpers.GetHashCode(obj);
        }
    }

    public class JSONSerializer
    {
        Hashtable objectInUse = null;

        public JSONObject Serialize(object o)
        {
            return SerializeValue(o);
        }

        private JSONObject SerializeClass(object o)
        {
            JSONObject result = new JSONObject();

            Type type = o.GetType();
            result.Add("_type", type.FullName);

            FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (FieldInfo field in fields)
            {
                result.Add(field.Name, SerializeValue(field.GetValue(o)));
            }

            PropertyInfo[] props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
            foreach (PropertyInfo prop in props)
            {
                MethodInfo getterInfo = prop.GetGetMethod();
                if (getterInfo == null)
                {
                    continue;
                }

                if (getterInfo.GetParameters().Length > 0)
                {
                    continue;
                }

                result.Add(prop.Name, SerializeValue(prop.GetValue(o, null)));
            }

            return result;
        }

        private JSONObject SerializeValue(object o)
        {
            if (o == null)
            {
                return new JSONLiteralNode("null");
            }

            string os = o as String;
            if (os != null)
            {
                return SerializeString(os);
            }

            if (o is Char)
            {
                return SerializeString(o.ToString());
            }

            if (o is bool)
            {
                return SerializeBoolean((bool)o);
            }

            if (o is DateTime)
            {
                return SerializeDateTime((DateTime)o);
            }

            if (o is DateTimeOffset)
            {
                return SerializeDateTime(((DateTimeOffset)o).UtcDateTime);
            }

            if (o is Guid)
            {
                return SerializeString(((Guid)o).ToString());
            }

            if (o is double)
            {
                return SerializeNumber((double)o);
            }

            if (o is float)
            {
                return SerializeNumber((float)o);
            }

            Type type = o.GetType();

            if (type.IsPrimitive || o is Decimal)
            {
                IConvertible convertible = o as IConvertible;
                if (convertible != null)
                {
                    return new JSONLiteralNode(convertible.ToString(CultureInfo.InvariantCulture));
                }
                else
                {
                    return new JSONLiteralNode(o.ToString());
                }
            }

            if (type.IsEnum)
            {
                Type underlyingType = Enum.GetUnderlyingType(type);
                if (underlyingType == typeof(Int64) || underlyingType == typeof(UInt64))
                {
                    // TODO: throw exceptions
                }
                return new JSONLiteralNode(((Enum)o).ToString("D"));
            }

            try
            {
                if (objectInUse == null)
                {
                    objectInUse = new Hashtable(new ReferenceComparator());
                }
                else if (objectInUse.ContainsKey(o))
                {
                    // TODO: Write exception message
                    throw new InvalidOperationException("TODO: Create exception message");
                }
                objectInUse.Add(o, null);

                IDictionary od = o as IDictionary;
                if (od != null)
                {
                    return SerializeDictionary(od);
                }

                IEnumerable oe = o as IEnumerable;
                if (oe != null)
                {
                    return SerializeEnumerable(oe);
                }

                return SerializeClass(o);
            }
            finally
            {
                if (objectInUse != null)
                {
                    objectInUse.Remove(o);
                }
            }

            throw new Exception("No converter yet");
        }

        private JSONLiteralNode SerializeNumber(double d)
        {
            return new JSONLiteralNode(d.ToString("r", CultureInfo.InvariantCulture));
        }
        private JSONLiteralNode SerializeNumber(float d)
        {
            return new JSONLiteralNode(d.ToString("r", CultureInfo.InvariantCulture));
        }

        private JSONObject SerializeDateTime(DateTime d)
        {
            return new JSONTextNode(d.ToString("o"));
        }

        private JSONArray SerializeEnumerable(IEnumerable e)
        {
            JSONArray result = new JSONArray();

            foreach (object o in e)
            {
                result.Add(Serialize(o));
            }

            return result;
        }

        private JSONObject SerializeDictionary(IDictionary d)
        {
            JSONObject result = new JSONObject();

            foreach (DictionaryEntry entry in d)
            {
                string key = entry.Key as string;
                if (key == null)
                {
                    // TODO: throw unsuported key exception
                    continue;
                }
                result.Add(key, Serialize(d[key]));
            }

            return result;
        }

        private JSONLiteralNode SerializeBoolean(bool b)
        {
            return new JSONLiteralNode(b ? "true" : "false");
        }

        private JSONLiteralNode SerializeString(string s)
        {
            if (s == null)
            {
                return new JSONLiteralNode("null");
            }
            else
            {
                return new JSONTextNode(s);
            }
        }

        private bool CheckIgnoreAttribute(MemberInfo info)
        {
            return (info.IsDefined(typeof(JSONIgnore), true));
        }
    }
}
