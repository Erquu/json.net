﻿using JSONdotNET;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONTest
{
    [TestClass]
    public class JSONArrayTest
    {
        [TestMethod]
        public void JSONArrayTest_Length()
        {
            JSONArray array = new JSONArray();
            array.Add("value_1");

            Assert.AreEqual(array.Length, 1);

            array.Add("value_2");
            array.Add("value_3");

            Assert.AreEqual(array.Length, 3);
        }

        [TestMethod]
        public void JSONArrayTest_Clear()
        {
            JSONArray array = new JSONArray();
            array.Add("value_1");
            array.Add("value_2");
            array.Add("value_3");

            Assert.AreEqual(array.Length, 3);

            array.Clear();

            Assert.AreEqual(array.Length, 0);

            array.Add("value_1");

            Assert.AreEqual(array.Length, 1);
        }

        [TestMethod]
        public void JSONArrayTest_IEnumerator_MoveNext()
        {
            JSONArray array = new JSONArray();
            array.Add("value_1");
            array.Add("value_2");
            array.Add("value_3");

            int counter = 0;

            while (array.MoveNext())
            {
                JSONObject o = array.Current;
                Assert.AreSame(o, array[counter]);
                counter++;
            }
        }

        [TestMethod]
        public void JSONArrayTest_IEnumerator_Foreach()
        {
            JSONArray array = new JSONArray();
            array.Add("value_1");
            array.Add("value_2");
            array.Add("value_3");

            int counter = 0;

            foreach (JSONObject o in array)
            {
                Assert.AreSame(o, array[counter]);
                counter++;
            }
        }
    }
}
