﻿using JSONdotNET;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONTest
{
    [TestClass]
    public class JSONParserTest
    {
        [TestMethod]
        public void ParserTest_Object_Normal()
        {
            string json = "{\"key1\":\"value1\",\"key2\":\"value2\"}";
            JSONParser parser = new JSONParser();
            JSONObject root = parser.Parse(json);

            Assert.AreEqual(root.GetString("key1"), "value1");
            Assert.AreEqual(root.GetString("key2"), "value2");
        }

        [TestMethod]
        public void ParserTest_Object_Deep()
        {
            /* {
             *   "key1": [
             *     "value1",
             *     {
             *       "foo": "bar"
             *     },
             *     "value2"
             *   ],
             *   "key2": "value2"
             * }
             */
            string json = "{\"key1\":[\"value1\",{\"foo\":\"bar\"},\"value2\"],\"key2\":\"value3\"}";
            JSONParser parser = new JSONParser();
            JSONObject root = parser.Parse(json);

            JSONArray key1 = root["key1"] as JSONArray;
            Assert.AreEqual(key1.Length, 3);
            Assert.AreEqual(key1[0].ToString(), "value1");
            Assert.AreEqual(key1[1].GetString("foo"), "bar");
            Assert.AreEqual(key1[2].ToString(), "value2");
            Assert.AreEqual(root.GetString("key2"), "value3");
        }
    }
}
