﻿using JSONdotNET;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;
using System.Threading;

namespace JSONTest
{
    class TokenTest
    {
        public string value;
        public JSONToken token;

        public TokenTest(JSONToken token, string value)
        {
            this.token = token;
            this.value = value;
        }

        public static bool Test(TokenTest[] tests, string json)
        {
            return Test(tests, json, JSONParserSettings.NONE);
        }
        public static bool Test(TokenTest[] tests, string json, JSONParserSettings settings) {
            int pointer = 0;

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json, settings);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                if (tokenizer.Token != tests[pointer].token || tokenizer.TokenValue != tests[pointer].value)
                {
                    return false;
                }
                pointer++;
            }

            return true;
        }
    }

    [TestClass]
    public class JSONTokenizerTest
    {
        [TestMethod]
        public void TokenizerTest_Simple()
        {
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(new MemoryStream(Encoding.UTF8.GetBytes("{\"key\":\"value\"}")), JSONParserSettings.AUTO_CLOSE_SOURCE);

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                Console.WriteLine(token.ToString());
            }
        }

        [TestMethod]
        public void TokenizerTest_EscapedDigit()
        {
            //, true, false, null, 123, 0.123, -123, -0.123e-10, -123.0e+10
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize("[\"\\u0040\"]");

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                Console.WriteLine("{0}: {1}", token.ToString(), tokenizer.TokenValue);
            }
        }

        [TestMethod]
        public void TokenizerTest_Boolean_True()
        {
            //, true, false, null, 123, 0.123, -123, -0.123e-10, -123.0e+10
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize("[true]");

            bool success = false;

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                if (token == JSONToken.FIELD_VALUE_TRUE && "true".Equals(tokenizer.TokenValue))
                {
                    success = true;
                }
            }

            Assert.IsTrue(success);
        }

        [TestMethod]
        public void TokenizerTest_Boolean_False()
        {
            //, true, false, null, 123, 0.123, -123, -0.123e-10, -123.0e+10
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize("[false]");

            bool success = false;

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                if (token == JSONToken.FIELD_VALUE_FALSE && "false".Equals(tokenizer.TokenValue))
                {
                    success = true;
                }
            }

            Assert.IsTrue(success);
        }

        [TestMethod]
        public void TokenizerTest_Boolean_Null()
        {
            //, true, false, null, 123, 0.123, -123, -0.123e-10, -123.0e+10
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize("[null]");

            bool success = false;

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                if (token == JSONToken.FIELD_VALUE_NULL && "null".Equals(tokenizer.TokenValue))
                {
                    success = true;
                }
            }

            Assert.IsTrue(success);
        }

        [TestMethod]
        public void TokenizerTest_Digit_Simple()
        {
            //, true, false, null, 123, 0.123, -123, -0.123e-10, -123.0e+10
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize("[123456]");

            bool success = false;

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                if (token == JSONToken.FIELD_VALUE_INTEGER && "123456".Equals(tokenizer.TokenValue))
                {
                    success = true;
                }
            }

            Assert.IsTrue(success);
        }

        [TestMethod]
        public void TokenizerTest_Digit_Negative()
        {
            //, true, false, null, 123, 0.123, -123, -0.123e-10, -123.0e+10
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize("[-123456]");

            bool success = false;

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                if (token == JSONToken.FIELD_VALUE_INTEGER && "-123456".Equals(tokenizer.TokenValue))
                {
                    success = true;
                }
            }

            Assert.IsTrue(success);
        }

        [TestMethod]
        public void TokenizerTest_Digit_Decimal_Simple()
        {
            //, true, false, null, 123, 0.123, -123, -0.123e-10, -123.0e+10
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize("[12.3456]");

            bool success = false;

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                if (token == JSONToken.FIELD_VALUE_DOUBLE && "12.3456".Equals(tokenizer.TokenValue))
                {
                    success = true;
                }
            }

            Assert.IsTrue(success);
        }

        [TestMethod]
        public void TokenizerTest_Digit_Decimal_Simple_Negative()
        {
            //, true, false, null, 123, 0.123, -123, -0.123e-10, -123.0e+10
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize("[-12.3456]");

            bool success = false;

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                if (token == JSONToken.FIELD_VALUE_DOUBLE && "-12.3456".Equals(tokenizer.TokenValue))
                {
                    success = true;
                }
            }

            Assert.IsTrue(success);
        }

        [TestMethod]
        public void TokenizerTest_Digit_Decimal_Big()
        {
            //, true, false, null, 123, 0.123, -123, -0.123e-10, -123.0e+10
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize("[-0.123e-05569]");

            bool success = false;

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE)
            {
                if (token == JSONToken.FIELD_VALUE_DOUBLE && "-0.123e-05569".Equals(tokenizer.TokenValue))
                {
                    success = true;
                }
            }

            Assert.IsTrue(success);
        }
    
        [TestMethod]
        public void TokenizerTest_Object_Normal()
        {
            string json = "{\"key1\":\"value1\",\"key2\":\"value2\"}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.FIELD_NAME, "key1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.FIELD_NAME, "key2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_Array_Normal()
        {
            string json = "[\"value1\",\"value2\",\"value3\",true,213.444,false]";
            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_ARRAY, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value3"),
                new TokenTest(JSONToken.FIELD_VALUE_TRUE, "true"),
                new TokenTest(JSONToken.FIELD_VALUE_DOUBLE, "213.444"),
                new TokenTest(JSONToken.FIELD_VALUE_FALSE, "false"),
                new TokenTest(JSONToken.END_ARRAY, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_Array_InnerObject()
        {
            string json = "[\"value1\",{\"innerKey1\":\"innerValue1\"},\"value3\",true,213.444,false]";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_ARRAY, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.FIELD_NAME, "innerKey1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "innerValue1"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value3"),
                new TokenTest(JSONToken.FIELD_VALUE_TRUE, "true"),
                new TokenTest(JSONToken.FIELD_VALUE_DOUBLE, "213.444"),
                new TokenTest(JSONToken.FIELD_VALUE_FALSE, "false"),
                new TokenTest(JSONToken.END_ARRAY, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_Push_Array_Normal()
        {
            string json = "[\"value1\",\"value2\",\"value3\",true,213.444,false]";
            JSONTokenizer tokenizer = new JSONTokenizer();

            int pointer = 0;
            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_ARRAY, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value3"),
                new TokenTest(JSONToken.FIELD_VALUE_TRUE, "true"),
                new TokenTest(JSONToken.FIELD_VALUE_DOUBLE, "213.444"),
                new TokenTest(JSONToken.FIELD_VALUE_FALSE, "false"),
                new TokenTest(JSONToken.END_ARRAY, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            bool fail = false;
            CountdownEvent ce = new CountdownEvent(tests.Length);

            tokenizer.TokenFound += delegate(object sender, JSONToken token)
            {
                if (tokenizer != sender || tests[pointer++].token != token)
                {
                    fail = true;
                }

                ce.Signal();
            };

            tokenizer.Tokenize(json, JSONParserSettings.TOKEN_PUSH);

            ce.Wait();

            Assert.IsFalse(fail);
        }

        [TestMethod]
        public void TokenizerTest_Push_Array_Async()
        {
            string json = "[\"value1\",\"value2\",\"value3\",true,213.444,false]";
            JSONTokenizer tokenizer = new JSONTokenizer();

            int pointer = 0;
            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_ARRAY, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value3"),
                new TokenTest(JSONToken.FIELD_VALUE_TRUE, "true"),
                new TokenTest(JSONToken.FIELD_VALUE_DOUBLE, "213.444"),
                new TokenTest(JSONToken.FIELD_VALUE_FALSE, "false"),
                new TokenTest(JSONToken.END_ARRAY, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            bool fail = false;
            CountdownEvent ce = new CountdownEvent(tests.Length);

            Thread starterThread = Thread.CurrentThread;

            tokenizer.TokenFound += delegate(object sender, JSONToken token)
            {
                if (starterThread != Thread.CurrentThread && (tokenizer != sender || tests[pointer++].token != token))
                {
                    fail = true;
                }

                ce.Signal();
            };

            tokenizer.Tokenize(json, JSONParserSettings.ASYNC);

            ce.Wait();

            Assert.IsFalse(fail);
        }

        [TestMethod]
        public void TokenizerTest_Array_Normal_Whitespaces()
        {
            string json = "[ \"value1\", \"value2\"] ";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_ARRAY, null),
                new TokenTest(JSONToken.WHITESPACE, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.WHITESPACE, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.END_ARRAY, null),
                new TokenTest(JSONToken.WHITESPACE, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json, JSONParserSettings.HANDLE_WHITESPACE))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_Push_Array_Normal_Whitespaces()
        {
            string json = "[ \"value1\", \"value2\"] ";
            JSONTokenizer tokenizer = new JSONTokenizer();

            int pointer = 0;
            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_ARRAY, null),
                new TokenTest(JSONToken.WHITESPACE, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.WHITESPACE, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.END_ARRAY, null),
                new TokenTest(JSONToken.WHITESPACE, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            bool fail = false;
            CountdownEvent ce = new CountdownEvent(tests.Length);

            tokenizer.TokenFound += delegate(object sender, JSONToken token)
            {
                if (tokenizer != sender || tests[pointer++].token != token)
                {
                    fail = true;
                }

                ce.Signal();
            };

            tokenizer.Tokenize(json, JSONParserSettings.TOKEN_PUSH | JSONParserSettings.HANDLE_WHITESPACE);

            ce.Wait();

            Assert.IsFalse(fail);
        }

        [TestMethod]
        public void TokenizerTest_SinglelineComment()
        {
            string json = "{\n\t//Comment for key1\n\t\"key1\": \"value1\",\n\t//Comment for key2\n\t\"key2\": \"value2\"\n}";
            
            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.COMMENT, "Comment for key1"),
                new TokenTest(JSONToken.FIELD_NAME, "key1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.COMMENT, "Comment for key2"),
                new TokenTest(JSONToken.FIELD_NAME, "key2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json, JSONParserSettings.ALLOW_C_LIKE_LINE_COMMENTS))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_SinglelineComment_Empty()
        {
            string json = "{\n\t//\n\t\"key1\": \"value1\",\n\t//\n\t\"key2\": \"value2\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.COMMENT, ""),
                new TokenTest(JSONToken.FIELD_NAME, "key1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.COMMENT, ""),
                new TokenTest(JSONToken.FIELD_NAME, "key2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json, JSONParserSettings.ALLOW_C_LIKE_LINE_COMMENTS))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_MultilineComment_Simple()
        {
            string json = "{\n\t/*\n\tA very long\n\tcomment for key 2\n\t*/\n\t\"key2\": \"value2\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.COMMENT, "\n\tA very long\n\tcomment for key 2\n\t"),
                new TokenTest(JSONToken.FIELD_NAME, "key2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json, JSONParserSettings.ALLOW_C_LIKE_MULTILINE_COMMENTS))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_MultilineComment()
        {
            string json = "{\n\t/*Comment for key1*/\n\t\"key1\": \"value1\",\n\t/*\n\tA very long\n\tcomment for key 2\n\t*/\n\t\"key2\": \"value2\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.COMMENT, "Comment for key1"),
                new TokenTest(JSONToken.FIELD_NAME, "key1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.COMMENT, "\n\tA very long\n\tcomment for key 2\n\t"),
                new TokenTest(JSONToken.FIELD_NAME, "key2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json, JSONParserSettings.ALLOW_C_LIKE_MULTILINE_COMMENTS))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_MultilineComment_Empty()
        {
            string json = "{\n\t/**/\n\t\"key1\": \"value1\",\n\t/**/\n\t\"key2\": \"value2\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.COMMENT, ""),
                new TokenTest(JSONToken.FIELD_NAME, "key1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.COMMENT, ""),
                new TokenTest(JSONToken.FIELD_NAME, "key2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json, JSONParserSettings.ALLOW_C_LIKE_MULTILINE_COMMENTS))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_MultilineComment_Asterisk()
        {
            string json = "{\n\t/*\n\t*Comment for key 1\n\t*/\n\t\"key1\": \"value1\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.COMMENT, "\n\t*Comment for key 1\n\t"),
                new TokenTest(JSONToken.FIELD_NAME, "key1"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json, JSONParserSettings.ALLOW_C_LIKE_MULTILINE_COMMENTS))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_MultilineComment_Closed_Late()
        {
            string json = "{\n\t/*\n\t\"key1\": \"value1\",\n\t/**/\n\t\"key2\": \"value2\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.COMMENT, "\n\t\"key1\": \"value1\",\n\t/*"),
                new TokenTest(JSONToken.FIELD_NAME, "key2"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value2"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json, JSONParserSettings.ALLOW_C_LIKE_MULTILINE_COMMENTS))
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TokenizerTest_Multiline_String()
        {
            string json = "{\n\t\"key\": \"foo\nbar\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.FIELD_NAME, "key"),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "foo\nbar"),
                new TokenTest(JSONToken.END_OBJECT, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json, JSONParserSettings.ALLOW_MULTILINE_STRINGS))
            {
                Assert.Fail();
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_SinglelineComment_Fail()
        {
            string json = "{\n\t//Comment for key1\n\t\"key1\": \"value1\",\n\t//Comment for key2\n\t\"key2\": \"value2\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null)
            };

            if (!TokenTest.Test(tests, json))
            {
                Assert.Fail();
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_MultilineComment_Fail()
        {
            string json = "{\n\t/*Comment for key1*/\n\t\"key1\": \"value1\",\n\t/*\n\tA very long\n\tcomment for key 2\n\t*/\n\t\"key2\": \"value2\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null)
            };

            if (!TokenTest.Test(tests, json))
            {
                Assert.Fail();
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_Multiline_String_Fail()
        {
            string json = "{\n\t\"key\": \"foo\nbar\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null),
                new TokenTest(JSONToken.FIELD_NAME, "key")
            };

            if (!TokenTest.Test(tests, json))
            {
                Assert.Fail();
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_MultilineComment_Not_Closed()
        {
            string json = "{\n\t/*\n\t\"key1\": \"value1\",\n\t\"key2\": \"value2\"\n}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_OBJECT, null)
            };

            TokenTest.Test(tests, json, JSONParserSettings.ALLOW_C_LIKE_MULTILINE_COMMENTS);

            Assert.Fail();
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_WrongArrayClosingTag()
        {
            string json = "[\"value1\"}";

            TokenTest[] tests = new TokenTest[] {
                new TokenTest(JSONToken.START_ARRAY, null),
                new TokenTest(JSONToken.FIELD_VALUE_STRING, "value1"),
                new TokenTest(JSONToken.END_ARRAY, null),
                new TokenTest(JSONToken.END_OF_FILE, null)
            };

            if (!TokenTest.Test(tests, json))
            {
                Assert.Fail();
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_WrongObjectClosingTag()
        {
            string json = "{\"key1\":\"value1\"]";

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE) ;
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_LiteralKey()
        {
            string json = "{null:\"value1\"]";

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE) ;
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_ArrayKeyValue()
        {
            string json = "[\"key1\":\"value1\"]";

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                Console.WriteLine("{0}: {1}", tokenizer.Token, tokenizer.TokenValue);
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_DoubleLiteralValue_Object()
        {
            string json = "{\"key1\":-0.20e-315true}";

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                Console.WriteLine("{0}: {1}", tokenizer.Token, tokenizer.TokenValue);
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_DoubleLiteralValue_Array()
        {
            string json = "[-0.20e-315true, truetrue, falsetrue, null0.123e4]";

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                Console.WriteLine("{0}: {1}", tokenizer.Token, tokenizer.TokenValue);
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_DoubleString_Object()
        {
            string json = "{\"key1\":\"Foo\" \"Bar\"}";

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                Console.WriteLine("{0}: {1}", tokenizer.Token, tokenizer.TokenValue);
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_DoubleString_Array()
        {
            string json = "[\"Foo\" \"Bar\"]";

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                Console.WriteLine("{0}: {1}", tokenizer.Token, tokenizer.TokenValue);
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_Empty_Value_Array()
        {
            string json = "[,\"foo\"]";

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                Console.WriteLine("{0}: {1}", tokenizer.Token, tokenizer.TokenValue);
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_Empty_Value_Variation_Array()
        {
            string json = "[\"foo\",]";

            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(json);

            while (tokenizer.NextToken() != JSONToken.END_OF_FILE)
            {
                Console.WriteLine("{0}: {1}", tokenizer.Token, tokenizer.TokenValue);
            }
        }

        [TestMethod, ExpectedException(typeof(JSONParserException))]
        public void TokenizerTest_TooManyClosingTags()
        {
            JSONTokenizer tokenizer = new JSONTokenizer();
            tokenizer.Tokenize(new MemoryStream(Encoding.UTF8.GetBytes("{\"key\":\"value\"}}}")), JSONParserSettings.AUTO_CLOSE_SOURCE);

            JSONToken token;
            while ((token = tokenizer.NextToken()) != JSONToken.END_OF_FILE) ;
        }
    }
}
