﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JSONdotNET.Serialization;
using JSONdotNET;

namespace JSONTest.Serialization
{
    [TestClass]
    public class JSONDeserializerTest
    {
        [TestMethod]
        public void Deserializer_SimplePropertyClassTest()
        {
            SimplePropertyClass spc = new SimplePropertyClass();
            spc.Prop_String = "string - test";
            spc.Prop_Float = 10.0f;
            spc.Prop_Bool = false;

            JSONSerializer serializer = new JSONSerializer();
            JSONObject spcSerialized = serializer.Serialize(spc);

            JSONDeserializer deserializer = new JSONDeserializer();
            SimplePropertyClass scpDeserialized = deserializer.Deserialize<SimplePropertyClass>(spcSerialized);

            Assert.IsTrue(
                spc.Prop_String == scpDeserialized.Prop_String &&
                spc.Prop_Float == scpDeserialized.Prop_Float &&
                spc.Prop_Bool == scpDeserialized.Prop_Bool);
        }

        [TestMethod]
        public void Deserializer_SubSimplePropertyClassTest()
        {
            SimplePropertyClass spc = new SimplePropertyClass();
            spc.Prop_String = "string - test";
            spc.Prop_Float = 10.0f;
            spc.Prop_Bool = false;

            SubSimpleClass ssc = new SubSimpleClass();
            ssc.Prop_SimpleClass = spc;

            JSONSerializer serializer = new JSONSerializer();
            JSONObject sscSerialized = serializer.Serialize(ssc);

            JSONDeserializer deserializer = new JSONDeserializer();
            SubSimpleClass sscDeserialized = deserializer.Deserialize<SubSimpleClass>(sscSerialized);

            Assert.IsTrue(
                sscDeserialized.Prop_SimpleClass != null &&
                spc.Prop_String == sscDeserialized.Prop_SimpleClass.Prop_String &&
                spc.Prop_Float == sscDeserialized.Prop_SimpleClass.Prop_Float &&
                spc.Prop_Bool == sscDeserialized.Prop_SimpleClass.Prop_Bool);
        }
    }
}
