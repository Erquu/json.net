﻿using JSONdotNET;
using JSONdotNET.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JSONTest.Serialization
{
    [TestClass]
    public class JSONSerializerTest
    {
        [TestMethod]
        public void Serializer_SimplePropertyTest()
        {
            SimplePropertyClass spc = new SimplePropertyClass();
            spc.Prop_String = "string - test";
            spc.Prop_Float = 10.0f;
            spc.Prop_Bool = false;

            JSONSerializer s = new JSONSerializer();
            JSONObject o = s.Serialize(spc);

            Assert.AreEqual("{\"_type\":\"JSONTest.Serialization.SimplePropertyClass\",\"Prop_String\":\"string - test\",\"Prop_Float\":10,\"Prop_Bool\":false}", o.ToString());
        }

        [TestMethod]
        public void Serializer_SubSimplePropertyTest()
        {
            SimplePropertyClass spc = new SimplePropertyClass();
            spc.Prop_String = "string - test";
            spc.Prop_Float = 10.0f;
            spc.Prop_Bool = false;

            SubSimpleClass ssc = new SubSimpleClass();
            ssc.Prop_SimpleClass = spc;

            JSONSerializer s = new JSONSerializer();
            JSONObject o = s.Serialize(ssc);

            Assert.AreEqual("{\"_type\":\"JSONTest.Serialization.SubSimpleClass\",\"Prop_SimpleClass\":{\"_type\":\"JSONTest.Serialization.SimplePropertyClass\",\"Prop_String\":\"string - test\",\"Prop_Float\":10,\"Prop_Bool\":false}}", o.ToString());
        }

        [TestMethod]
        public void Serializer_NullablePrimitiveTest_Normal()
        {
            NullablePrimitivePropertyClass nppc = new NullablePrimitivePropertyClass();
            nppc.Prop_NullableInt = 22;
            
            JSONSerializer s = new JSONSerializer();
            JSONObject o = s.Serialize(nppc);

            Assert.AreEqual("{\"_type\":\"JSONTest.Serialization.NullablePrimitivePropertyClass\",\"Prop_NullableInt\":22}", o.ToString());
        }

        [TestMethod]
        public void Serializer_NullablePrimitiveTest_Null()
        {
            NullablePrimitivePropertyClass nppc = new NullablePrimitivePropertyClass();
            nppc.Prop_NullableInt = null;

            JSONSerializer s = new JSONSerializer();
            JSONObject o = s.Serialize(nppc);

            Assert.AreEqual("{\"_type\":\"JSONTest.Serialization.NullablePrimitivePropertyClass\",\"Prop_NullableInt\":null}", o.ToString());
        }

        [TestMethod]
        public void Serializer_SimpleFieldTest()
        {
            SimpleFieldsClass sfc = new SimpleFieldsClass();
            sfc.Field_String = "testö äü 22";
            sfc.Field_Byte = (byte)128;
            sfc.Field_Float = 0.0000000001201f;

            JSONSerializer s = new JSONSerializer();
            JSONObject o = s.Serialize(sfc);

            JSONPrettifierSettings settings = JSONPrettifierSettings.DEFAULT;
            settings.Minified = true;
            settings.ConvertNonASCII = false;

            Assert.AreEqual("{\"_type\":\"JSONTest.Serialization.SimpleFieldsClass\",\"Field_String\":\"testö äü 22\",\"Field_Float\":1.201E-10,\"Field_Byte\":128}", o.ToString(settings));
        }

        [TestMethod]
        public void Serializer_StringTest()
        {
            string test = "Hello World";

            JSONSerializer s = new JSONSerializer();
            JSONObject o = s.Serialize(test);

            JSONPrettifierSettings settings = JSONPrettifierSettings.DEFAULT;
            settings.Minified = true;
            settings.ConvertNonASCII = false;

            Assert.AreEqual("\"Hello World\"", o.ToString(settings));
        }
    }
}
