﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONTest.Serialization
{
    public class SimplePropertyClass
    {
        public string Prop_String { get; set; }
        public float Prop_Float { get; set; }
        public bool Prop_Bool { get; set; }
    }

    public class NullablePrimitivePropertyClass
    {
        public int? Prop_NullableInt { get; set; }
    }

    public class SubSimpleClass
    {
        public SimplePropertyClass Prop_SimpleClass { get; set; }
    }

    public class SimpleFieldsClass
    {
        public string Field_String;
        public float Field_Float;
        public byte Field_Byte;
    }
}
